export enum SwitchStatus {
	ON = 'ON', OFF = 'OFF', UNKNOWN = 'UNKNOWN'
}

export interface Switch {
	id: string;
	name: string;
	status: SwitchStatus;
}

export interface Room {
	id: string;
	name: string;
	temperature: number;
	humidity: number;
	switches: Array<Switch>;
}