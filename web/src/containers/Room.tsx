import React, { Component } from 'react';
import io from 'socket.io-client';
import * as model from '../model/Api';
import Thermometer from '../components/Thermometer';
import Flash from '../components/flash';
import SwitchContainer from './SwitchContainer';

const socket = io();

interface RoomProps {
	room: model.Room;
}

interface RoomState {
	temperature: number;
  feeling: TempFeeling;
  message: string;
}

enum TempFeeling {
  COLD = 'C', SLIGHTLY_COLD = 'SC', NICE = 'N', SLIGHTLY_WARM = 'SW', WARM = 'W'
}

export default class Room extends Component<RoomProps, RoomState> {
  private flashRef: any;

  constructor(props: RoomProps) {
		super(props);
		this.state = {
			temperature: props.room.temperature,
      feeling: TempFeeling.NICE,
      message: '',
		}
	}

  async save() {
    const body = { roomId: this.props.room.id, feeling: this.state.feeling};
    await fetch('/api/feelings', {method: 'post', headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }, body: JSON.stringify(body)});
    this.flashRef.flash('Obrigado! Vamos tentar ajustar a melhor temperatura para você');
  }

  optionChanged(event: React.FormEvent<HTMLSelectElement>) {
    const newOption = event.currentTarget.value as TempFeeling;
    this.setState({ feeling: newOption });
  }

	componentDidMount() {
		socket.on(`tempStatus/${this.props.room.id}`, (data: string) => {
			const status = JSON.parse(data);
			this.setState({ temperature: status.temperature });
		});
	}

  setFlashRef(ref: any) {
    this.flashRef = ref;
  }

	render() {
		const { room } = this.props;
		return (
			<div>
				<h2>{room.name}</h2>
        <Flash ref={this.setFlashRef.bind(this)} />
				<Thermometer temperature={this.state.temperature} />
				{room.switches.map(control => (
					<SwitchContainer key={control.id} control={control} room={room} />
				))}
        <div>
          <label htmlFor="feeling">Como está a temperatura agora?</label>
          <select id="feeling" onChange={this.optionChanged.bind(this)} value={this.state.feeling}>
            <option value={TempFeeling.COLD}>Frio</option>
            <option value={TempFeeling.SLIGHTLY_COLD}>Friozinho</option>
            <option value={TempFeeling.NICE}>Normal</option>
            <option value={TempFeeling.SLIGHTLY_WARM}>Um pouco quente</option>
            <option value={TempFeeling.WARM}>Muito Quente</option>
          </select>
          <button onClick={this.save.bind(this)}>Salvar</button>
        </div>
			</div>
		)
	}
}
