import React, { Component } from "react";
import Switch from "../components/Switch";
import * as model from "../model/Api";
import io from 'socket.io-client';

const socket = io();

interface ContainerProps {
	control: model.Switch;
	room: model.Room;
}
interface ContainerState {
	status: model.SwitchStatus;
}

export default class SwitchContainer extends Component<ContainerProps, ContainerState> {

	constructor(props: ContainerProps) {
		super(props);
		this.state = {
			status: props.control.status || model.SwitchStatus.UNKNOWN,
		}
	}

	componentDidMount() {
		socket.on(`deviceStatus/${this.props.room.id}/${this.props.control.id}`, (data: string) => {
			const status = JSON.parse(data) as model.SwitchStatus;
			this.setState({ status });
		});
	}

	render() {
		return <Switch status={this.state.status} />
	}
}