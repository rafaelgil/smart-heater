import React from 'react';
import './Thermometer.css';

interface ThermProps {
	temperature: number;
}

export default ({ temperature }: ThermProps) => {
	let marginTop = 100 - temperature;
	let height = temperature;
	if (marginTop < 0) {
		marginTop = 0;
	}
	if (height > 100) {
		height = 100;
	}
	return (
		<div className="thermometer">
			<div className="fill" style={{ marginTop, height }}>
				<p className="label">{temperature}ºC</p>
			</div>
		</div>
	);
};