import React from 'react';
import { Switch, SwitchStatus } from '../model/Api';
import './Switch.css';

interface SwitchProps {
	status: SwitchStatus;
}

export default ({ status }: SwitchProps) =>
	<i className="fas fa-fire heater-icon"
		style={{ color: status === SwitchStatus.ON ? 'orange' : 'gray' }} />;