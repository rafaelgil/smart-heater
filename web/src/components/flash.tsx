import React, {Component} from 'react';

const TIMEOUT = 5000;

interface FlashProps {
}

interface FlashState {
  show: boolean;
  message: string;
}

export default class Flash extends Component<FlashProps, FlashState> {

  constructor(props: FlashProps) {
    super(props);
    this.state = {
      show: false,
      message: ''
    };
  }

  public flash(message: string) {
    this.setState({
      show:true,
      message
    });
  }

  render() {
      const { show, message } = this.state;
      if (show) {
        setTimeout(() => this.setState({ show: false }), TIMEOUT);
        return <div>{message}</div>;
      }
      return null;
  }
}
