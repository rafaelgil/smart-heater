import React, { Component } from 'react';
import Room from './containers/Room';
import * as model from './model/Api';
import './App.css';

interface AppProps {
}

interface AppState {
  loading: boolean;
  rooms: Array<model.Room>;
}

export default class App extends Component<AppProps, AppState> {

  constructor(props: AppProps) {
    super(props);
    this.state = {
      loading: true,
      rooms: [],
    };
  }

  async componentDidMount() {
    const response = await fetch('/api/rooms');
    const rooms = await response.json();
    this.setState({ rooms, loading: false });
  }

  render() {
    if (this.state.loading) {
      return <div>Loading...</div>;
    }

    const rooms = this.state.rooms.map(room => <Room key={room.id} room={room} />);
    return (
      <div className="App">
        {rooms}
      </div>
    );
  }
}
