const DEBUG_ENABLED = true;

if (DEBUG_ENABLED) {
  const log = document.createElement('div');
  log.id = 'debugDiv';
  const label = document.createElement('span');
  label.textContent = 'Console';
  log.appendChild(label);
  ['log','debug','info','warn','error'].forEach(function (verb) {
    console[verb] = (function (method, verb, log) {
      return function () {
        method.apply(console, arguments);
        const msg = document.createElement('div');
        msg.classList.add(verb);
        msg.textContent = verb + ': ' + Array.prototype.slice.call(arguments).join(' ');
        log.appendChild(msg);
      };
    })(console[verb], verb, log);
  }); 
  document.body.appendChild(log);
  console.info('Estamos debugando');
}
