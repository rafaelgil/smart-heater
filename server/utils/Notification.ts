import https from 'https';

const URL = 'https://maker.ifttt.com/trigger/notify_mobile/with/key/c650OOZruDLHqJH6jPONFv'

export async function sendNotification(message: string) {
  try {
    return https.get(`${URL}?value1=${message}`);
  } catch (e) {
    console.error(`Failed to send the notification with message: ${message}`);
  }
}
