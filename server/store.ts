import Room, { RoomStatus } from "./model/Room";

interface UpdateRoom {
  niceTemp?: number;
  status?: RoomStatus;
}

const state = {
  rooms: [] as Array<Room>
};

export function getRooms() {
  return state.rooms;
}

export function findRoom(id: string) {
  return state.rooms.find(room => room.id === id);
}

export function addRoom(room: Room) {
  state.rooms = [...state.rooms, room];
}

export function updateRoom(id: string, params: UpdateRoom) {
  state.rooms = state.rooms.map((r) => {
    if (r.id === id) {
      // I know, mutation is bad, but there is no much benefit here for the complexity of cloning a room.
      // TODO: Change the room to a simple object that can be easily cloned
      r.niceTemp = params.niceTemp || r.niceTemp;
      r.status = params.status || r.status;
      // r.offset = 0;
    }
    return r;
  })
}
