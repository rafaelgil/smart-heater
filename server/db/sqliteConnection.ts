import { Database } from 'sqlite3';

const conn = new Database('./db/database.db',  (err) => {
  if (err) {
    console.error(err.message);
  }
});

export async function queryAll(sql: string, params?: any): Promise<Array<any>> {
  return new Promise((resolve, reject) => {
    conn.all(sql, params, (err, rows) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

export async function runQuery(sql: string, params?: any): Promise<void> {
  return new Promise((resolve, reject) => {
    conn.run(sql, params, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    })
  })
}

export default conn;
