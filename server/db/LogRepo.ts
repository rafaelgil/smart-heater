import { DBLog } from './entities';
import { runQuery, queryAll } from './sqliteConnection';

export async function all(): Promise<Array<DBLog>> {
  return (await queryAll('SELECT * FROM LOGS'))
    .map(row => ({ timestamp: row.created_at, log: row.log }));
}

export async function addLog(log: string) {
  const query = 'INSERT INTO LOGS (log) VALUES (?)';
  runQuery(query, [log]);
}
