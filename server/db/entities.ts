import { AvailableSensorTypes } from '../sensors';
import { AvailableDeviceTypes } from '../devices';

export interface DBSchedule {
  startTime: string;
  endTime: string;
  roomTemp: number;
}

export interface DBDevice {
  id?: string;
  type: AvailableDeviceTypes;
  name: string;
  urlOn?: string;
  urlOff?: string;
  gpioPin?: number;
}

export interface DBSensor {
  id?: string;
  name: string;
  type: AvailableSensorTypes;
  url: string;
  topic: string;
  macAddress: string;
}

export interface DBRoom {
  id: string;
  name: string;
  schedule: Array<DBSchedule> 
  devices: Array<DBDevice>;
  sensor: DBSensor;
}

export interface DBLog {
  timestamp: number;
  log: string;
}

export enum TempFeelingEnum {
  COLD = 'C', SLIGHTLY_COLD = 'SC', NICE = 'N', SLIGHTLY_WARM = 'SW', WARM = 'W'
}

export interface DBFeeling {
  id?: string;
  roomId: string;
  timestamp: number;
  realTemp: number;
  heatIndex: number;
  humidity: number;
  feeling: TempFeelingEnum; 
}
