PRAGMA foreign_keys = ON;
PRAGMA temp_store = 2; -- Store temp tables in memory

CREATE TEMP TABLE _VARIABLES (key TEXT, VALUE INTEGER);

CREATE TABLE IF NOT EXISTS SENSORS (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name VARCHAR(255),
  type VARCHAR(255),
  url VARCHAR(255),
  topic VARCHAR(255),
  mac_address VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS ROOMS (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name VARCHAR(255),
  sensor_id INTEGER,
  FOREIGN KEY (sensor_id) REFERENCES SENSORS(id)
);

CREATE TABLE IF NOT EXISTS SCHEDULE (
  start_time VARCHAR(5),
  end_time VARCHAR(5),
  room_temp DECIMAL(2,1),
  room_id INTEGER,
  FOREIGN KEY (room_id) REFERENCES ROOMS(id)
);

CREATE TABLE IF NOT EXISTS DEVICES (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  type VARCHAR(255),
  name VARCHAR(255),
  url_on VARCHAR(255),
  url_off VARCHAR(255),
  gpio_pin INTEGER,
  room_id INTEGER,
  FOREIGN KEY (room_id) REFERENCES ROOMS(id)
);

CREATE TABLE IF NOT EXISTS LOGS (
  created_at TIMESTAMP DEFAULT (datetime('now', 'localtime')),
  log VARCHAR(1000)
);

CREATE TABLE FEELINGS (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  room_id INTEGER, 
  created_at TIMESTAMP,
  real_temp DECIMAL(2,1),
  heat_index DECIMAL(2,1),
  humidity DECIMAL(2,1),
  feeling VARCHAR(255),
  FOREIGN KEY (room_id) REFERENCES ROOMS(id)
);

INSERT INTO SENSORS (name, type, mac_address)
VALUES ('Xiaomi Thermometer', 'xiaomi', '58:2d:34:38:18:df');

INSERT INTO ROOMS (name, sensor_id)
VALUES ('Casa', (SELECT last_insert_rowid()));

INSERT INTO _VARIABLES (key, value) VALUES ('room', (SELECT last_insert_rowid()));

-- INSERT INTO DEVICES (type, name, url_on, url_off, room_id)
-- VALUES ('cloud', 'Sonoff', 'https://maker.ifttt.com/trigger/turn_on/with/key/c650OOZruDLHqJH6jPONFv', 'https://maker.ifttt.com/trigger/turn_off/with/key/c650OOZruDLHqJH6jPONFv',
-- (SELECT value from _VARIABLES where key = 'room'));

INSERT INTO DEVICES (type, name, url_on, url_off, room_id)
VALUES ('cloud', 'Notification', 'https://maker.ifttt.com/trigger/notify_mobile/with/key/c650OOZruDLHqJH6jPONFv?value1=Heater is On', 'https://maker.ifttt.com/trigger/notify_mobile/with/key/c650OOZruDLHqJH6jPONFv?value1=Heater is Off', (SELECT value from _VARIABLES where key = 'room'));

INSERT INTO DEVICES (type, name, gpio_pin, room_id)
VALUES ('gpio', 'GPIO #23', 16, (SELECT value from _VARIABLES where key = 'room'));

INSERT INTO SCHEDULE (start_time, end_time, room_temp, room_id)
VALUES ('09:30', '21:59', 22, (SELECT value from _VARIABLES where key = 'room'));
INSERT INTO SCHEDULE (start_time, end_time, room_temp, room_id)
VALUES ('22:00', '23:59', 21, (SELECT value from _VARIABLES where key = 'room'));
INSERT INTO SCHEDULE (start_time, end_time, room_temp, room_id)
VALUES ('00:00', '09:29', 16, (SELECT value from _VARIABLES where key = 'room'));

DROP TABLE _VARIABLES;
