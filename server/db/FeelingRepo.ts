import storage from 'node-persist';
import { DBFeeling, TempFeelingEnum } from './entities';
import uuid from 'uuid/v1';
import Room from '../model/Room';

export async function save(feeling: TempFeelingEnum, room: Room) { 
  const dbFeeling = {
    id: uuid(),
    roomId: room.id,
    timestamp: Date.now(),
    realTemp: room.sensor.control.temperature,
    heatIndex: room.sensor.control.heatIndex,
    humidity: room.sensor.control.humidity,
    feeling
  } as DBFeeling;
  const currentFeelings = (await storage.getItem('feelings')) || [];
  const newFeelings = [dbFeeling];
  return storage.setItem('feelings', [...currentFeelings, ...newFeelings]);
}

export async function all() {
  return storage.getItem('feelings');
}
