// import uuid from 'uuid/v1';
import { DBRoom } from './entities';
import { queryAll, runQuery } from './sqliteConnection';

const mapSchedule = (row: any) => ({
  startTime: row.start_time,
  endTime: row.end_time,
  roomTemp: row.room_temp,
});

const mapDevice = (row: any) => ({
  id: row.id,
  type: row.type,
  name: row.name,
  urlOn: row.url_on,
  urlOff: row.url_off,
  gpioPin: row.gpio_pin,
});

export async function all(): Promise<Array<DBRoom>> {
  const sql = 'SELECT ROOMS.*, SENSORS.name as sensor_name, SENSORS.type as sensor_type, SENSORS.url as sensor_url, SENSORS.topic as sensor_topic, SENSORS.mac_address as sensor_mac_address from ROOMS JOIN SENSORS ON ROOMS.sensor_id = SENSORS.id';
  const rooms = await queryAll(sql);
  return Promise.all(rooms.map(async (room: any) => {
    const { id, name } = room;
    const scheduleQuery = `SELECT * FROM SCHEDULE WHERE room_id = ${id}`;
    const deviceQuery = `SELECT * FROM DEVICES WHERE room_id = ${id}`;
    const schedule = (await queryAll(scheduleQuery)).map(mapSchedule);
    const devices = (await queryAll(deviceQuery)).map(mapDevice);
    return {
      id: `${id}`, // Always keeps id as string
      name,
      schedule,
      devices,
      sensor: {
        id: room.sensor_id,
        name: room.sensor_name,
        type: room.sensor_type,
        url: room.sensor_url,
        topic: room.sensor_topic,
        macAddress: room.sensor_mac_address,
      }
    };
  }));
}

export async function save(room: DBRoom) {
  const insertQuery = 'INSERT INTO ROOMS (name, sensor_id) VALUES (?,?)'
  const updateQuery = 'UPDATE ROOMS SET name = ?, sensor_id = ? WHERE id = ?';
  if (room.sensor && room.name) {
    if (room.id) {
      await runQuery(updateQuery, [room.name, room.sensor.id, room.id]);
    } else {
      await runQuery(insertQuery, [room.name, room.sensor.id]);
    }
  }
}

export async function initDB() {
  // await storage.init();
  // // await storage.setItem('rooms', null);
  // const rooms = await all();
  // if (rooms) {
  //   return Promise.resolve(null);
  // }
  // const API_KEY = 'c650OOZruDLHqJH6jPONFv';
  // const IFTTT_URL = 'https://maker.ifttt.com/trigger/';
  // // const cloudDevice = {
  // //   id: uuid(),
  // //   type: 'cloud',
  // //   name: 'Sonoff',
  // //   urlOn: `${IFTTT_URL}turn_on/with/key/${API_KEY}`,
  // //   urlOff: `${IFTTT_URL}turn_off/with/key/${API_KEY}`
  // // };

  // const gpioDevice = {
  //   id: uuid(),
  //   type: 'gpio',
  //   name: 'GPIO #23',
  //   gpioPin: 16,
  // } as DBDevice;

  // const notifDevice = {
  //   id: uuid(),
  //   type: 'cloud',
  //   name: 'Notification',
  //   urlOn: `${IFTTT_URL}notify_mobile/with/key/${API_KEY}?value1=Heater is On`,
  //   urlOff: `${IFTTT_URL}notify_mobile/with/key/${API_KEY}?value1=Heater is Off`
  // } as DBDevice;

  // const xiaomiSensor = {
  //   id: uuid(),
  //   name: 'Xiaomi Thermometer',
  //   type: 'xiaomi',
  //   macAddress: '58:2d:34:38:18:df',
  // } as DBSensor;

  // const room1 = {
  //   id: uuid(),
  //   name: 'Quarto',
  //   schedule: [
  //     { startTime: '09:30', endTime: '23:59', roomTemp: 22 },
  //     { startTime: '00:00', endTime: '09:29', roomTemp: 16 }
  //     // { startTime: '13:35', endTime: '13:36', roomTemp: 70 },
  //     // { startTime: '13:37', endTime: '21:59', roomTemp: 86 }
  //   ],
  //   devices: [gpioDevice, notifDevice],
  //   sensor: xiaomiSensor,
  // };
  // // const room2 = {
  // //   name: 'Sala Real',
  // //   niceTemp: 22,
  // //   devices: [dbDevice, notifDevice],
  // //   sensor: {
  // //     id: uuid(),
  // //     type: 'arduino',
  // //     name: 'Arduino DHT',
  // //     url: '',
  // //     topic: ''
  // //   }
  // // };
  // await save(room1);
  // // await save(room2);
}
