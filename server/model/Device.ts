import { SwitchDevice } from "../devices/SwitchDevice";

export default class Device {
  public readonly id?: string;
  public readonly name: string;
  public readonly control: SwitchDevice;

  constructor(id: string | undefined, name: string, control: SwitchDevice) {
    this.id = id;
    this.name = name;
    this.control = control;
  }
}
