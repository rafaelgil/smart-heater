import Device from "./Device";
import Sensor from "./Sensor";

export enum RoomStatus {
	STARTED = 'STARTED', STOPPED = 'STOPPED'
}

export default class Room {
  public readonly id: string;
  public readonly name: string;
  public readonly devices: Array<Device>;
  public readonly sensor: Sensor;
  private privateNiceTemp: number;
  private privateStatus: RoomStatus;
  private privateOffset = 0.5;

  constructor(id: string, name: string, devices: Array<Device>, sensor: Sensor, niceTemp: number) {
    this.id = id;
    this.name = name;
    this.devices = devices;
    this.sensor = sensor;
    this.privateNiceTemp = niceTemp;
    this.privateStatus = RoomStatus.STOPPED;
  }

  get niceTemp() {
    return this.privateNiceTemp;
  }

  set niceTemp(niceTemp: number) {
    this.privateNiceTemp = niceTemp;
  }

  get status() {
    return this.privateStatus;
  }

  set status(status: RoomStatus) {
    this.privateStatus = status;
  }

  get offset() {
    return this.privateOffset;
  }
 
  set offset(offset: number) {
    this.privateOffset = offset;
  }

  /**
   * This method removes all listeners and turns off all devices.
   */
  stop() {
    const sensor = this.sensor.control;
    const switchDevices = this.devices.map(device => device.control);
    sensor.removeAllListeners();
    switchDevices.forEach(switchDevice => switchDevice.turnOff());
    this.devices.forEach(device => {
      device.control.removeAllListeners();
    });
  }
}
