import { TemperatureSensor } from "../sensors/TemperatureSensor";

export default class Sensor {
  constructor(public id: string | undefined, public name: string, public control: TemperatureSensor) {}
}
