export interface TemperatureStatus {
  readonly temperature: number;
  readonly humidity: number;
  readonly heatIndex?: number;
  readonly failed?: boolean;
  readonly batteryLevel?: number;
}

export interface TemperatureSensor {
  readonly temperature: number;
  readonly humidity: number;
  readonly heatIndex?: number;
  readonly failed: boolean;
  readonly batteryLevel?: number;
  onStatusChange(listener: (message: TemperatureStatus) => void): void;
  removeAllListeners(): void;
  start(): void;
  stop(): void;
}

export abstract class AbstractTemperatureSensor implements TemperatureSensor {
  public temperature = 0;
  public humidity = 0;
  public heatIndex = 0;
  public failed = false;
  public batteryLevel = 0;
  private listeners: Array<(message: TemperatureStatus) => void>;

  constructor() {
    this.listeners = [];
  }

  protected sendMessage() {
    this.listeners.forEach(listener => {
      listener({
        temperature: this.temperature,
        humidity: this.humidity,
        heatIndex: this.heatIndex,
        failed: this.failed,
        batteryLevel: this.batteryLevel
      });
    });
  }

  public onStatusChange(listener: (message: TemperatureStatus) => void) {
    this.listeners.push(listener);
  }

  public removeAllListeners() {
    this.listeners = [];
  }

  public start() {}

  public stop() {}
}
