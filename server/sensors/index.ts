import MqttTempSensor from '../sensors/MqttTempSensor';
import ArduinoSensor from '../sensors/Arduino';
import XiaomiBLE from '../sensors/XiaomiBLE';
import DHTSimulator from '../sensors/DHTSimulator';
import { DBRoom } from '../db/entities';
import Device from '../model/Device';
import Sensor from '../model/Sensor';

export type AvailableSensorTypes = 'mqtt' | 'sim' | 'arduino' | 'xiaomi';
const sensorTypes = {
  mqtt: MqttTempSensor,
  sim: DHTSimulator,
  arduino: ArduinoSensor,
  xiaomi: XiaomiBLE,
}

export function getSensor(dbRoom: DBRoom, devices: Array<Device>): Sensor {
  const control = new sensorTypes[dbRoom.sensor.type](dbRoom, devices);
  return new Sensor(dbRoom.sensor.id, dbRoom.sensor.name, control);
}
