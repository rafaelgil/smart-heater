import { AbstractTemperatureSensor } from "./TemperatureSensor";
import mqtt, { MqttClient } from 'mqtt';
import { DBRoom } from "../db/entities";

export default class MqttTempSensor extends AbstractTemperatureSensor {
  private client: MqttClient;
  private topic: string;

  constructor(dbRoom: DBRoom) {
    super();
    const topic = dbRoom.sensor.topic;
    const brokerUrl = dbRoom.sensor.url;
    this.topic = topic;
    this.client = mqtt.connect(brokerUrl);
  }

  private onMessage(topic: string, message: string) {
    this.temperature = parseFloat(message.toString());
    this.sendMessage();
  }

  public start() {
    this.client.on('connect', () => this.client.subscribe(this.topic));
    this.client.on('message', this.onMessage.bind(this));
  }

  public stop() {
    this.client.removeAllListeners();
  }
}
