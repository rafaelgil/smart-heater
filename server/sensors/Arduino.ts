import { AbstractTemperatureSensor } from "./TemperatureSensor";
import SerialPort from 'serialport';
import { parsers } from 'serialport';

/**
 * Reads JSON data from serial sent by an arduino on /dev/ttyACM0
 */
export default class ArduinoSensor extends AbstractTemperatureSensor {

  private serial: SerialPort | undefined;

  start() {
    try {
      this.serial = new SerialPort('/dev/ttyACM0', {
        baudRate: 9600
      });

      const readline = new parsers.Readline({ delimiter: '\n' });
      this.serial.pipe(readline);
      readline.on('data', (data: string) => {
        try {
          const info = JSON.parse(data);
          this.humidity = info.humidity;
          this.temperature = info.tempC;
          this.heatIndex = info.heatIndex;
          this.sendMessage();
        } catch (e) {
          console.log('Incomplete JSON received. Will try again...');
        }
      });
    } catch (e) {
      console.log('Could not connect to Arduino device on /dev/ttyACM0');
    }
  }

  stop() {
    if (this.serial) {
      this.serial.close();
    }
  }
}
