import { AbstractTemperatureSensor } from "./TemperatureSensor";
import { DeviceStatus } from "../devices/SwitchDevice";
import Device from "../model/Device";
import { DBRoom } from '../db/entities';

const TIME_HEAT = 5000;
const TIME_COLD = 10000;

export default class DHTSimulator extends AbstractTemperatureSensor {

  private offInterval: NodeJS.Timeout | undefined;
  private onInterval: NodeJS.Timeout | undefined;

  constructor(room: DBRoom, devices: Array<Device>) {
    super();
    this.temperature = 22;
    this.humidity = 84;
    const switches = devices.map(dev => dev.control);
    switches.forEach(device =>  {
      device.onStatusChange((status) => {
        if (status === DeviceStatus.ON) {
          this.clearInterval();
          this.onInterval = setInterval(() => {
            if (this.temperature < 125) {
              this.temperature += 0.5;
            }
            this.sendMessage();
          }, TIME_HEAT);
        } else if (status === DeviceStatus.OFF) {
          this.clearInterval();
          this.offInterval = setInterval(() => {
            if (this.temperature > 0) {
              this.temperature -= 0.5;
            }
            this.sendMessage();
          }, TIME_COLD);
        }
      });
    });
  }

  private clearInterval() {
    if (this.offInterval) {
      clearInterval(this.offInterval);
    }
    if (this.onInterval) {
      clearInterval(this.onInterval);
    }
  }
}
