import { AbstractTemperatureSensor } from "./TemperatureSensor";
import { spawn } from 'child_process';
import { DBRoom } from "../db/entities";
import { sendNotification } from '../utils/Notification';

const RESTART_ATTEMPTS = 10;
const MAX_ATTEMPTS = 50;
const POLL_TIMEOUT = 30000;

export default class XiaomiBLE extends AbstractTemperatureSensor {

  private attempts = 0;
  private timeout: NodeJS.Timeout | null = null;
  private macAddress = '';
  private room: DBRoom;

  constructor(dbRoom: DBRoom) {
    super();
    this.room = dbRoom;
    this.macAddress = dbRoom.sensor.macAddress;
    this.temperature = 999; // Starts "shutted down"
  }

  restartBluetoothService() {
    console.log('Restarting bluetooth service...');
    const process = spawn('sudo', ['service', 'bluetooth', 'restart']);
    process.on('close', (code) => {
      console.log(`Finished with code ${code}`);
    })
  }

  poll() {
    const pyProcess = spawn('python3', ['/home/pi/mitemp/get_json.py', '--backend', 'bluepy', 'poll', this.macAddress]);
    pyProcess.stdout.on('data', (data) => {
      const parsed = JSON.parse(data);
      this.temperature = parsed.temperature
      this.humidity = parsed.humidity
      this.sendMessage();
    });
    pyProcess.on('close', (code) => {
      if (code > 0) {
        if (this.attempts > MAX_ATTEMPTS) {
          this.temperature = 999; // Forces shut down
          this.failed = true;
          sendNotification(`Sensor of room ${this.room.name} failed.`);
          this.sendMessage();
        } else {
          if (this.attempts > RESTART_ATTEMPTS) {
            this.restartBluetoothService(); // This is Async, but it should be faster than the timeout. If not, consequences of not waiting are not very bad.
          }
          this.timeout = setTimeout(this.poll.bind(this), POLL_TIMEOUT);
          this.attempts += 1;
        }
      } else {
        this.attempts = 0;
        this.timeout = setTimeout(this.poll.bind(this), POLL_TIMEOUT);
      }
    });
  }

  stop() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  }

  start() {
    this.failed = false;
    this.attempts = 0;
    this.poll();
  }
}
