const API_KEY = 'c650OOZruDLHqJH6jPONFv';
const IFTTT_URL = 'https://maker.ifttt.com/trigger/';
const dbDevice = {
	id: 1,
	type: 'cloud',
	name: 'Sonoff',
	urlOn: `${IFTTT_URL}turn_on/with/key/${API_KEY}`,
	urlOff: `${IFTTT_URL}turn_off/with/key/${API_KEY}`
};

const notifDevice = {
    id: 2,
    type: 'cloud',
    name: 'Notification',
    urlOn: `${IFTTT_URL}notify_mobile/with/key/${API_KEY}?value1=Time to turn on the heat!`,
    urlOff:`${IFTTT_URL}notify_mobile/with/key/${API_KEY}?value1=You can turn off your heater now!` 
};

export default [
	{
		id: 1,
		name: 'Sala',
		niceTemp: 23,
		devices: [],
		sensor: {
			id: 1,
			name: 'Xiaomi',
			type: 'mqtt',
			topic: 'temp1',
			url: 'https://test.mosquitto.org'
		}
	},
	{
		id: 2,
		name: 'Sala Real',
		niceTemp: 22,
	  devices: [notifDevice],
		sensor: {
			id: 2,
			type: 'arduino',
			name: 'Arduino DHT',
			url: '',
			topic: ''
		}
	}
];
