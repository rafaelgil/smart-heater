import { SwitchDevice, DeviceStatus } from "./SwitchDevice";
import { addLog } from '../db/LogRepo';
import { promise as gpio } from 'rpi-gpio';
import { DBDevice } from "../db/entities";

export default class GPIODevice implements SwitchDevice {

  public status: DeviceStatus;
  private listeners: Array<(status: DeviceStatus) => void>;
  private pin: number;
  private timeTracker: number;

  constructor(dbDevice: DBDevice) {
    this.status = DeviceStatus.OFF;
    gpio.setup(dbDevice.gpioPin, gpio.DIR_LOW);
    this.pin = dbDevice.gpioPin || 0;
    this.timeTracker = 0;
    this.listeners = [];
  }

  public async turnOn() {
    await gpio.write(this.pin, true);
    this.setStatus(DeviceStatus.ON);
    this.timeTracker = Date.now();
    addLog(`Heater on GPIO pin #${this.pin} is ON.`);
    return this.status;
  }

  public async turnOff() {
    await gpio.write(this.pin, false);
    this.setStatus(DeviceStatus.OFF);
    addLog(`Heater on GPIO pin #${this.pin} is OFF. Were ON for ${(Date.now() - this.timeTracker) / 1000 / 60} minutes`)
    return this.status;
  }

  private setStatus(status: DeviceStatus) {
    this.status = status;
    this.listeners.forEach(listener => {
      listener(this.status);
    });
  }

  public onStatusChange(listener: (status: DeviceStatus) => void) {
    this.listeners.push(listener);
  }

  public removeAllListeners() {
    this.listeners = [];
  }
}
