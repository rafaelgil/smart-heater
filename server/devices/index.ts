import { DBDevice } from '../db/entities';
import Device from '../model/Device';
import GPIODevice from './GPIODevice';
import CloudDevice from './CloudDevice';

export type AvailableDeviceTypes = 'gpio' | 'cloud';
const deviceTypes = {
  gpio: GPIODevice,
  cloud: CloudDevice,
}

export function getDevice(dbDevice: DBDevice): Device {
  const control =  new deviceTypes[dbDevice.type](dbDevice);
  return new Device(dbDevice.id, dbDevice.name, control);
}
