import { SwitchDevice, DeviceStatus } from "./SwitchDevice";
import https from 'https';
import { DBDevice } from "../db/entities";

export default class CloudDevice implements SwitchDevice {

  public status: DeviceStatus;
  private listeners: Array<(status: DeviceStatus) => void>;
  private urlOn: string;
  private urlOff: string;

  constructor(dbDevice: DBDevice) {
    this.status = DeviceStatus.UNKNOWN;
    this.urlOn = dbDevice.urlOn || '';
    this.urlOff = dbDevice.urlOff || '';
    this.listeners = [];
  }

  private async makeRequest(url: string) {
    try {
      return https.get(url);
    } catch (e) {
      console.error('Failed to complete the request.');
    }
    return null;
  }

  public async turnOn() {
    await this.makeRequest(this.urlOn);
    this.setStatus(DeviceStatus.ON);
    return this.status;
  }

  public async turnOff() {
    await this.makeRequest(this.urlOff);
    this.setStatus(DeviceStatus.OFF);
    return this.status;
  }

  private setStatus(status: DeviceStatus) {
    this.status = status;
    this.listeners.forEach(listener => {
      listener(this.status);
    });
  }

  public onStatusChange(listener: (status: DeviceStatus) => void) {
    this.listeners.push(listener);
  }

  public removeAllListeners() {
    this.listeners = [];
  }
}
