export enum DeviceStatus {
	ON = 'ON', OFF = 'OFF', UNKNOWN = 'UNKNOWN'
}

export interface SwitchDevice {
	readonly status: DeviceStatus;
	turnOn(): Promise<DeviceStatus>;
	turnOff(): Promise<DeviceStatus>;
	onStatusChange(listener: (status: DeviceStatus) => void): void;
  removeAllListeners(): void;
}
