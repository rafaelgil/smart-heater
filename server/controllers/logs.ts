import express = require('express');
import * as repo from '../db/LogRepo';

export function all(req: express.Request, res: express.Response) {
  repo.all()
    .then((logs) => res.send(logs))
    .catch(() => res.send([]));
}
