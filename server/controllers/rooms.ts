import { Socket } from 'socket.io';
import express = require('express');
import { DBRoom } from '../db/entities';
import { TemperatureStatus } from '../sensors/TemperatureSensor';
import Room, { RoomStatus } from '../model/Room';
import * as RoomRepo from '../db/RoomRepo';
import { getRooms, findRoom, updateRoom } from '../store';
import { DeviceStatus } from '../devices/SwitchDevice';

const DEFAULT_OFFSET = 0.5;

const roomMap = (room: Room) => ({
  id: room.id,
  name: room.name,
  niceTemp: room.niceTemp,
  temperature: room.sensor.control.temperature,
  sensorFailed: room.sensor.control.failed,
  humidity: room.sensor.control.humidity,
  switches: room.devices.map(device => ({
    id: device.id,
    name: device.name,
    status: device.control.status
  })),
  status: room.status,
});

export function index(req: express.Request, res: express.Response) {
  res.send(getRooms().map(roomMap));
}

export function getById(req: express.Request, res: express.Response) {
  const id = req.params.id;
  const room = getRooms().find(r => r.id === id);
  if (room) {
    res.send(roomMap(room));
  } else {
    res.sendStatus(404);
  }
}

export function save(req: express.Request, res: express.Response) {
  const id = req.params.id;
  const room = { id, ...req.body } as DBRoom;
  const respStatus = id ? 200 : 201;
  RoomRepo.save(room).then(() => {
    res.sendStatus(respStatus);
    updateRoom(id, { status: req.body.status, niceTemp: req.body.niceTemp });
  });
}

export const start = (sockets: Array<Socket>) => (req: express.Request, res: express.Response) => {
  const id = req.params.id;
  const room = findRoom(id);
  if (room && room.status !== RoomStatus.STARTED) {
    const switchDevices = room.devices.map(device => device.control);
    const sensor = room.sensor.control;
    sensor.start();

    const toggleHeater = (status: TemperatureStatus) => {
      const stateRoom = findRoom(id); // Always get an updated room from the store.
      if (stateRoom) {
        if (status.temperature <= stateRoom.niceTemp - stateRoom.offset) {
          switchDevices.forEach(switchDevice => switchDevice.status !== DeviceStatus.ON && switchDevice.turnOn());
          stateRoom.offset = DEFAULT_OFFSET;
        } else if (status.temperature > stateRoom.niceTemp) {
          switchDevices.forEach(switchDevice => switchDevice.status !== DeviceStatus.OFF && switchDevice.turnOff());
        }
      }
    };

    sensor.onStatusChange((status) => {
      toggleHeater(status);
      sockets.forEach(socket => socket.emit(`tempStatus/${room.id}`, JSON.stringify(status)));
    });

    room.devices.forEach(device => {
      device.control.onStatusChange((status) => {
        sockets.forEach(socket => socket.emit(`deviceStatus/${room.id}/${device.id}`, JSON.stringify(status)));
      });
    });

    toggleHeater({
      temperature: sensor.temperature,
      humidity: sensor.humidity,
    });
    updateRoom(room.id, { status: RoomStatus.STARTED });
    res.sendStatus(200);
  } else {
    res.sendStatus(404);
  }
}

export const stop = (sockets: Array<Socket>) => (req: express.Request, res: express.Response) => {
  const id = req.params.id;
  const room = findRoom(id);
  if (room && room.status !== RoomStatus.STOPPED) {
    room.stop();
    room.devices.forEach(device => {
      sockets.forEach(socket => socket.emit(`deviceStatus/${room.id}/${device.id}`, JSON.stringify(DeviceStatus.OFF)));
    });
    room.status = RoomStatus.STOPPED;
    res.sendStatus(200);
  } else {
    res.sendStatus(404);
  }
}
