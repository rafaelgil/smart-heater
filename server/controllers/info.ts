import express = require('express');

export function info(req: express.Request, res: express.Response) {
  res.send({ version:'1.2' });
}
