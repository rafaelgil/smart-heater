import express = require('express');
import { findRoom } from '../store';
import * as repo from '../db/FeelingRepo';

export function save(req: express.Request, res: express.Response) {
  const { feeling, roomId } = req.body;
  const room = findRoom(roomId);
  if (room) {
    repo.save(feeling, room).then(() => res.sendStatus(201));
  } else {
    res.sendStatus(404);
  }
}

export function all(req: express.Request, res: express.Response) {
  repo.all()
    .then((feelings) => res.send(feelings))
    .catch(() => res.send([]));
}
