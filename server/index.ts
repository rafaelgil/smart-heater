import socketIo, { Socket } from 'socket.io';
import { createServer } from 'http';
import express = require('express');
import bodyParser = require('body-parser');
import * as rooms from './controllers/rooms';
import * as logs from './controllers/logs';
import * as feeling from './controllers/feeling';
import * as info from './controllers/info';
import { addRoom } from './store';
import Room from './model/Room';
import * as RoomRepo from './db/RoomRepo';
import { getSensor } from './sensors';
import { getDevice } from './devices';
import { initSchedule } from './tools/scheduler';
import { DBRoom } from './db/entities';

const app = express();
const server = createServer(app);
const io = socketIo(server);
app.use(bodyParser.json());

const sockets: Array<Socket> = [];
io.on('connection', socket => {
  sockets.push(socket);
});

function getRoomTemp(dbRoom: DBRoom): number {
  const date = new Date();
  const time = (date.getHours() * 60) + date.getMinutes();
  return dbRoom.schedule.reduce((acc, schedule) => {
    const [startHour, startMinute] = schedule.startTime.split(':');
    const [endHour, endMinute] = schedule.endTime.split(':');
    const startTime = (parseInt(startHour) * 60) + parseInt(startMinute);
    const endTime = (parseInt(endHour) * 60) + parseInt(endMinute);
    if (time >= startTime && time < endTime) {
      return schedule.roomTemp;
    }
    return acc;
  }, -999); // If no schedule is found, turn off the devices.
}

async function init() {
  await RoomRepo.initDB();
  const dbRooms = await RoomRepo.all();

  dbRooms.forEach(dbRoom => {
    const devices = dbRoom.devices.map((dbDevice) => {
      return getDevice(dbDevice);
    });
    const sensor = getSensor(dbRoom, devices);

    // Store initialization:
    if (devices.length && sensor) {
      addRoom(new Room(dbRoom.id, dbRoom.name, devices, sensor, getRoomTemp(dbRoom)));
    }
  });
  initSchedule(dbRooms);
}

init();

// Routes:
app.get('/api/rooms', rooms.index);
app.get('/api/rooms/:id', rooms.getById);
app.patch('/api/rooms/:id', rooms.save);
app.post('/api/rooms/:id/stop', rooms.stop(sockets));
app.post('/api/rooms/:id/start', rooms.start(sockets));
app.post('/api/rooms', rooms.save);
app.post('/api/feelings', feeling.save);
app.get('/api/feelings', feeling.all);
app.get('/api/logs', logs.all);
app.get('/api/info', info.info);

server.listen(8080, () => {
  console.log('Server running on 8080');
});
