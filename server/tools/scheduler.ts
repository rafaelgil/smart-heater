import { schedule, ScheduledTask } from 'node-cron';
import { updateRoom } from '../store';
import { DBRoom } from '../db/entities';

let scheduledTasks: Array<ScheduledTask> = [];

export function initSchedule(rooms: Array<DBRoom>) {
  rooms.forEach((room) => {
    room.schedule.forEach((scheduleItem) => {
      const [startHour, startMinute] = scheduleItem.startTime.split(':');
      const [endHour, endMinute] = scheduleItem.endTime.split(':');
      const startTask = schedule(`${startMinute} ${startHour} * * *`, () => {
        updateRoom(room.id, { niceTemp: scheduleItem.roomTemp });
      });
      const endTask = schedule(`${endMinute} ${endHour} * * *`, () => {
        // updateRoom(room.id, { niceTemp: -999 }); // Turns off all devices on next sensor update
      });
      scheduledTasks.push(startTask, endTask);
    });
  });
}

export function clearSchedule() {
  scheduledTasks.forEach(task => task.destroy());
  scheduledTasks = [];
}
